#!/usr/bin/env python
 

import rospy, math, random
import numpy as np
import sensor_msgs.point_cloud2 
from sensor_msgs.msg import PointCloud2,PointField
from sensor_msgs.msg import LaserScan
from laser_geometry import LaserProjection
from std_msgs.msg import Header


class ObjectTrans:
    def __init__(self):
        self.gt_obj_sub = rospy.Subscriber("/points_map", PointCloud2, self.gt_obj_callback)
        self.pc_2=rospy.Publisher("/points_map_2", PointCloud2, queue_size=1)

        self.OFFSET_X=302459.942
        self.OFFSET_Y=4122635.537
    def gt_obj_callback(self,msg) :
        
        points = []
        for point in sensor_msgs.point_cloud2.read_points(msg,skip_nans=True):
            
            pt=[point[0]- self.OFFSET_X,point[1]- self.OFFSET_Y,point[2]-27]
            points.append(pt)


        
        header = Header()
        header.frame_id = "map"
        fields = [PointField('x', 0, PointField.FLOAT32, 1),
          PointField('y', 4, PointField.FLOAT32, 1),
          PointField('z', 8, PointField.FLOAT32, 1),
          ]
        pc2_msg=sensor_msgs.point_cloud2.create_cloud(header, fields, points)
        self.pc_2.publish(pc2_msg)
        print('finish changed')





if __name__ == '__main__':
     
    rospy.init_node('obj_trans', anonymous=True)

    test_trans = ObjectTrans()
    rospy.spin()

