#!/usr/bin/env python
 
import rospy
import numpy as np

from tf.transformations import quaternion_from_euler,euler_from_quaternion
from geometry_msgs.msg import PoseStamped,Quaternion,TwistStamped,Polygon,Point32
from math import atan2,pow,sqrt,pi,cos,sin
from autoware_msgs.msg import DetectedObjectArray,DetectedObject
from morai_msgs.msg import ObjectStatusList
from sensor_msgs.msg import PointCloud
from sensor_msgs.msg import PointCloud2

#/detection/objects
# /detection/lidar_detector/objects

class ObjectTrans:
    def __init__(self):
        self.current_pose=PoseStamped()
        self.is_pose=False
        self.gt_obj_sub = rospy.Subscriber("/Object_topic", ObjectStatusList, self.gt_obj_callback)
    
        self.pose_sub = rospy.Subscriber("/morai_pose", PoseStamped, self.pose_callback)


        self.obj_pub=rospy.Publisher("/morai_lidar_obj", DetectedObjectArray, queue_size=1)
        self.obj_point_pub=rospy.Publisher("/morai_lidar_point", PointCloud, queue_size=1)
       
        self.sensor_trans_x=0.6
        self.sensor_trans_y=0
        self.sensor_trans_z=1.0
    def pose_callback(self,msg) :
        self.current_pose=msg
        self.is_pose=True



    def gt_obj_callback(self,msg) :
        
        morai_point_msg=PointCloud()
        morai_point_msg.header.frame_id="world"
        morai_point_msg.header.stamp=rospy.Time.now()
        if self.is_pose :
            trans_msg=DetectedObjectArray()
            trans_msg.header.frame_id='velodyne'
            trans_msg.header.stamp=rospy.Time.now()
            
            object_num = msg.num_of_npcs + msg.num_of_pedestrian + msg.num_of_obstacle
	    object_list=[]
            for num in range(msg.num_of_npcs) :

	        tmp_x = msg.npc_list[num].position.x
	        tmp_y = msg.npc_list[num].position.y
	        tmp_z = msg.npc_list[num].position.z
                object_list.append([tmp_x,tmp_y,tmp_z])

            for num in range(msg.num_of_pedestrian) :

	        tmp_x = msg.pedestrian_list[num].position.x
	        tmp_y = msg.pedestrian_list[num].position.y
	        tmp_z = msg.pedestrian_list[num].position.z
                object_list.append([tmp_x,tmp_y,tmp_z])

            for num in range(msg.num_of_obstacle) :

	        tmp_x = msg.obstacle_list[num].position.x
	        tmp_y = msg.obstacle_list[num].position.y
	        tmp_z = msg.obstacle_list[num].position.z
                object_list.append([tmp_x,tmp_y,tmp_z])
               

	    id=1
            for moraiObject in object_list :
		x=moraiObject[0]
		y=moraiObject[1]
                z=moraiObject[2]

                obj=DetectedObject()
                obj.dimensions.x=1.0
                obj.dimensions.y=1.0
                obj.dimensions.z=1.0
                obj.id=id
                id+=1
                obj.header.frame_id='velodyne'
                obj.header.stamp=rospy.Time.now()
                
                obj.pose.position.x=x
                obj.pose.position.y=y
                obj.pose.position.z=z
                obj.pose.orientation.w=1


                
                obj.convex_hull.header.frame_id=obj.header.frame_id
                obj.convex_hull.header.stamp= obj.header.stamp
                
                obj_point=Point32()
                obj_point.x=x
                obj_point.y=y
                obj_point.z=z
                obj.convex_hull.polygon.points.append(obj_point)

                tmp_point=Point32()
                tmp_point.x=x
                tmp_point.y=y
                tmp_point.z=z
                morai_point_msg.points.append(tmp_point)

                obj.score=1
               
                
      
                obj.space_frame='velodyne'
                trans_msg.objects.append(obj)
                print(num,x,y)
            
           
                
            self.obj_pub.publish(trans_msg)
            self.obj_point_pub.publish(morai_point_msg)



if __name__ == '__main__':
     
    rospy.init_node('obj_trans', anonymous=True)

    test_trans = ObjectTrans()
    rospy.spin()
